const express = require("express");
const auth = require("../middleware/auth");
const Comment = require("../models/Comment");

const router = express.Router();

const createRouter = () => {

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    Comment
    .find({post: id})
    .populate('user')
    .then(results => res.send(results))
    .catch(() => res.sendStatus(500));
  });

  router.post("/", auth, (req, res) => {
    const data = req.body;

    if (data.title === '') {
      res.status(400).send('Field "comment" can not be blank');
    } else {
      data.user = req.user._id;

      const comment = new Comment(data);
      comment
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
      }
  });

   return router;
};

module.exports = createRouter;
