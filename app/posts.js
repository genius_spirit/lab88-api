const express = require("express");
const multer = require("multer");
const nanoid = require("nanoid");
const path = require("path");
const auth = require("../middleware/auth");
const Post = require("../models/Post");
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

const router = express.Router();

const createRouter = () => {

  router.get("/", (req, res) => {
    Post.find()
    .populate('user')
    .sort({datetime: -1})
    .then(results => res.send(results))
    .catch(() => res.sendStatus(500));
  });

  router.post("/", [auth, upload.single("image")], (req, res) => {
    const postData = req.body;
    if (req.file) {
      postData.image = req.file.filename;
    } else {
      postData.image = null;
    }

    if (postData.description === '' && postData.image === null) {
      res.status(400).send('Fields description or image can not be blank');
    } else {
      postData.user = req.user._id;

      const post = new Post(postData);
        post
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    }
  });

  router.get("/:id", (req, res) => {
    const id = req.params.id;
    Post.findById(id)
    .then(result => {
      if (result) res.send(result);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
  });


  return router;
};

module.exports = createRouter;
